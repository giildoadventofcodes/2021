import fs from 'fs'
import path from 'path'

class NavigationSystem {
  constructor (entries) {
    this.entries = []
    entries.forEach(line => {
      if (line !== '') {
        this.entries.push(line.split(''))
      }
    })
  }

  linesChecker () {
    const openChars = ['(', '[', '{', '<']
    const closeChars = [')', ']', '}', '>']
    let checkChars = []
    let lastChar = ''

    return this.entries.reduce((acc, line) => {
      checkChars = []
      lastChar = ''
      for (const char of line) {
        if (openChars.includes(char)) {
          checkChars.push(char)
          continue
        }

        lastChar = checkChars.pop()

        switch (char) {
          case ')':
            if (lastChar === '(') continue
            return this.illegalCharPoint(char) + acc

          case ']':
            if (lastChar === '[') continue
            return this.illegalCharPoint(char) + acc

          case '}':
            if (lastChar === '{') continue
            return this.illegalCharPoint(char) + acc

          case '>':
            if (lastChar === '<') continue
            return this.illegalCharPoint(char) + acc
        }
      }

      return acc
    }, 0)
  }

  illegalCharPoint (illegalChar) {
    switch (illegalChar) {
      case ')':
        return 3
      case ']':
        return 57
      case '}':
        return 1197
      case '>':
        return 25137
    }
  }
}

export default () => {
  const navigationSystem = new NavigationSystem(
    fs.readFileSync(path.join(process.cwd(), '/data/day10.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  return navigationSystem.linesChecker()
}
