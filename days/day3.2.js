import fs from 'fs'
import path from 'path'

export default () => {
  let readBit = {
    '1': 0,
    '0': 0,
  }
  let oxygenEntries = []
  let dioxydeEntries = []
  let firstEntry = ''
  fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
    .split('\n')
    .forEach((bits, i) => {
      if (bits) {
        oxygenEntries.push(bits)
        dioxydeEntries.push(bits)
        if (i === 0) {
          firstEntry = bits
        }

        readBit[bits[0]]++
      }
    })

  for (let i = 0; i < firstEntry.length; i++) {
    if (i === 0) {
      oxygenEntries = oxygenEntries.filter(bits => readBit['0'] > readBit['1'] ? bits[i] === '0' : bits[i] === '1')
      dioxydeEntries = dioxydeEntries.filter(bits => readBit['0'] < readBit['1'] ? bits[i] === '0' : bits[i] === '1')
    } else {
      if (oxygenEntries.length > 1) {
        readBit = {
          '1': 0,
          '0': 0,
        }
        oxygenEntries.forEach(bits => {
          readBit[bits[i]]++
        })
        oxygenEntries = oxygenEntries.filter(bits => readBit['0'] > readBit['1'] ? bits[i] === '0' : bits[i] === '1')
      }
      if (dioxydeEntries.length > 1) {
        readBit = {
          '1': 0,
          '0': 0,
        }
        dioxydeEntries.forEach(bits => {
          readBit[bits[i]]++
        })
        dioxydeEntries = dioxydeEntries.filter(bits => readBit['0'] <= readBit['1'] ? bits[i] === '0' : bits[i] === '1')
      }
    }

    if (dioxydeEntries.length === 1 && oxygenEntries.length === 1) {
      break
    }
  }

  return parseInt(oxygenEntries[0], 2) * parseInt(dioxydeEntries[0], 2)
}
