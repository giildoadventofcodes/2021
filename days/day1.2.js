import fs from 'fs'
import path from 'path'

export default () => {
  const deeps = fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
                  .split('\n')
                  .map(value => parseInt(value))
                  .filter(value => !isNaN(value))
  let i = 0
  let beforeValue = null
  for (let j = 2; j < deeps.length; j++) {
    if (beforeValue) {
      const actualWindow = deeps[j] + deeps[j - 1] + deeps[j - 2]

      if (beforeValue < actualWindow) {
        i++
      }

      beforeValue = actualWindow
    } else {
      beforeValue = deeps[j] + deeps[j - 1] + deeps[j - 2]
    }
  }

  return i
}
