import fs from 'fs'
import path from 'path'

export default () => {
  let x = 0
  let y = 0
  fs.readFileSync(path.join(process.cwd(), '/data/day2.txt'), { encoding: 'utf8' })
    .split('\n')
    .forEach(enter => {
      const [command, value] = enter.split(' ')
      if (command && value) {
        switch (command) {
          case 'forward':
            x += parseInt(value)
            break
          case 'down':
            y += parseInt(value)
            break
          case 'up':
            y -= parseInt(value)
            break
        }
      }
    })
  return x * y
}
