import fs from 'fs'
import path from 'path'

export default () => {
  let gamma = ''
  let epsilon = ''
  let readBits = []
  fs.readFileSync(path.join(process.cwd(), '/data/day3.txt'), { encoding: 'utf8' })
    .split('\n')
    .forEach((bits, i) => {
      if (bits) {
        for (let j = 0; j < bits.length; j++) {
          if (i === 0) {
            readBits.push({
              '1': 0,
              '0': 0,
            })
          }
          readBits[j][bits[j]]++
        }
      }
    })

  readBits.forEach(bit => {
    const isZero = bit['0'] > bit['1']
    gamma += isZero ? '0' : '1'
    epsilon += isZero ? '1' : '0'
  })

  return parseInt(gamma, 2) * parseInt(epsilon, 2)
}
