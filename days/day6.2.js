import fs from 'fs'
import path from 'path'

class Fish {
  constructor (fishesList) {
    this.fishes = new Map()
    for (let i = 0; i < 9; i++) {
      this.fishes.set(i, 0)
    }
    fishesList.forEach(fish => {
      this.fishes.set(fish, this.fishes.get(fish) + 1)
    })
  }

  daySimulation (dayNumber) {
    let beforeNumber = 0
    let actualNumber = 0
    for (let i = 1; i <= dayNumber; i++) {
      for (let j = 8; j >= 0; j--) {
        if (j < 8) {
          actualNumber = this.fishes.get(j)
          this.fishes.set(j, beforeNumber)

          if (j === 0) {
            this.fishes.set(6, this.fishes.get(6) + actualNumber)
            this.fishes.set(8, actualNumber)
            beforeNumber = 0
            actualNumber = 0
          } else {
            beforeNumber = actualNumber
          }
        } else {
          beforeNumber = this.fishes.get(j)
        }
      }
    }
  }

  getFishNumber () {
    let fishNumber = 0
    for (const [_, number] of this.fishes) {
      fishNumber += number
    }

    return fishNumber
  }
}

export default () => {
  let fishesList = fs.readFileSync(path.join(process.cwd(), '/data/day6.txt'), { encoding: 'utf8' })
                     .split('\n')[0].split(',')
                                    .map(fish => parseInt(fish))

  const fishes = new Fish(fishesList)
  fishes.daySimulation(256)

  return fishes.getFishNumber()
}
