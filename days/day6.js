import fs from 'fs'
import path from 'path'

export default () => {
  let fishes = fs.readFileSync(path.join(process.cwd(), '/data/day6_test.txt'), { encoding: 'utf8' })
                 .split('\n')[0].split(',')
                                .map(fish => parseInt(fish))

  let newFishes = 0
  for (let i = 1; i <= 80; i++) {
    newFishes = 0
    fishes = fishes.map(fish => {
      if (fish === 0) {
        newFishes++
        return 6
      }

      return fish - 1
    })

    for (let j = 0; j < newFishes; j++) {
      fishes.push(8)
    }
  }

  return fishes.length
}
