import fs from 'fs'
import path from 'path'

export default () => {
  const deeps = fs.readFileSync(path.join(process.cwd(), '/data/day1.txt'), { encoding: 'utf8' })
                  .split('\n')
                  .map(value => parseInt(value))
                  .filter(value => !isNaN(value))
  let i = 0
  let beforeValue = null
  deeps.forEach(deepValue => {
    if (beforeValue && beforeValue < deepValue) {
      i++
    }

    beforeValue = deepValue
  })
  return i
}
