import fs from 'fs'
import path from 'path'

class SubmarineSubterraneanMap {
  constructor (entries) {
    this.subterraneans = new Map([
      ['start', { isBigSub: false, neighbors: new Set() }],
      ['end', { isBigSub: false, neighbors: new Set() }],
    ])
    this.littleSubterraneanAlreadyVisited = new Map()
    this.possibilitiesPath = new Set()
    for (const line of entries) {
      if (line !== '') {
        if (/^start-[a-zA-Z]+$/.test(line)) {
          const [_, subName] = line.split('-')
          this.addSub('start', subName, false)
          continue
        }

        if (/^[a-zA-Z]+-start$/.test(line)) {
          const [subName, _] = line.split('-')
          this.addSub('start', subName, false)
          continue
        }

        if (/^end-[a-zA-Z]+$/.test(line)) {
          const [_, subName] = line.split('-')
          this.addSub('end', subName, true)
          continue
        }

        if (/^[a-zA-Z]-end+$/.test(line)) {
          const [subName, _] = line.split('-')
          this.addSub('end', subName, true)
          continue
        }

        const [subNameA, subNameB] = line.split('-')
        this.addSub(subNameA, subNameB, true)
      }
    }
  }

  addSub (subA, subB, addReverse) {
    if (this.subterraneans.has(subA)) {
      const value = this.subterraneans.get(subA)
      this.subterraneans.set(
        subA,
        {
          ...value,
          neighbors: value.neighbors.add(subB),
        },
      )
    } else {
      this.subterraneans.set(
        subA,
        {
          isBigSub: /^[A-Z]+$/.test(subA),
          neighbors: new Set([subB]),
        },
      )
    }

    if (addReverse) {
      const value = this.subterraneans.get(subB)
      if (this.subterraneans.has(subB)) {
        this.subterraneans.set(
          subB,
          {
            ...value,
            neighbors: value.neighbors.add(subA),
          },
        )
      } else {
        this.subterraneans.set(
          subB,
          {
            isBigSub: /^[A-Z]+$/.test(subB),
            neighbors: new Set([subA]),
          },
        )
      }
    }
  }

  browseTheMap () {
    const startNeighbors = this.subterraneans.get('start').neighbors

    for (const startNeighbor of startNeighbors) {
      this.browseNeighborsSubterranean(startNeighbor, ['start', startNeighbor], true)
    }
  }

  littleSubHasAlreadyTwice () {
    for (const value of this.littleSubterraneanAlreadyVisited.values()) {
      if (value === 2) return true
    }

    return false
  }

  browseNeighborsSubterranean (subName, path, isStartSub) {
    const subNeighbors = this.subterraneans.get(subName).neighbors
    let subNeighborSettings = null
    for (const subNeighborName of subNeighbors) {
      if (isStartSub) {
        this.littleSubterraneanAlreadyVisited.clear()
        subNeighborSettings = this.subterraneans.get(subName)
        if (!subNeighborSettings.isBigSub) {
          this.littleSubterraneanAlreadyVisited.set(subName, 1)
        }
      }

      if (subNeighborName === 'end') {
        this.possibilitiesPath.add([...path, 'end'])
        continue
      }

      subNeighborSettings = this.subterraneans.get(subNeighborName)
      if (!subNeighborSettings.isBigSub &&
        (
          this.littleSubterraneanAlreadyVisited.get(subNeighborName) === 2 ||
          ( this.littleSubHasAlreadyTwice() && this.littleSubterraneanAlreadyVisited.get(subNeighborName) === 1)
        )
      ) {
        continue
      } else {
        if (!subNeighborSettings.isBigSub) {
          this.littleSubterraneanAlreadyVisited.set(
            subNeighborName,
            this.littleSubterraneanAlreadyVisited.has(subNeighborName) ? 2 : 1,
          )
        }
        this.browseNeighborsSubterranean(subNeighborName, [...path, subNeighborName], false)
        if (!subNeighborSettings.isBigSub) {
          this.littleSubterraneanAlreadyVisited.get(subNeighborName) === 2
          ? this.littleSubterraneanAlreadyVisited.set(subNeighborName, 1)
          : this.littleSubterraneanAlreadyVisited.delete(subNeighborName)
        }
      }
    }

    return
  }
}

export default () => {
  const submarineSubterraneanMap = new SubmarineSubterraneanMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day12.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  submarineSubterraneanMap.browseTheMap()
  const pathsUnique = new Set()
  submarineSubterraneanMap.possibilitiesPath.forEach(path => {
    console.log(JSON.stringify(path))
    pathsUnique.add(JSON.stringify(path))
  })
  return pathsUnique.size
}
