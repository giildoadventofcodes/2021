import fs from 'fs'
import path from 'path'

class CaveMap {
  constructor (entries) {
    this.chitonRisks = []
    this.open = new Map()
    this.close = new Map()

    let entryNumbers = null

    const incrementRisk = (entry, i) => {
      return entry.map(risk => {
        if (risk + i < 10) return risk + i

        return (risk + i) % 9
      })
    }

    for (let i = 0; i < 5; i++) {
      for (const entry of entries) {
        if (entry !== '') {
          entryNumbers = entry.split('')
                              .map(chitonRisk => parseInt(chitonRisk))
          this.chitonRisks.push([
            ...incrementRisk(entryNumbers, i),
            ...incrementRisk(entryNumbers, 1 + i),
            ...incrementRisk(entryNumbers, 2 + i),
            ...incrementRisk(entryNumbers, 3 + i),
            ...incrementRisk(entryNumbers, 4 + i),
          ])
        }
      }
    }

    this.maxX = this.chitonRisks[0].length
    this.maxY = this.chitonRisks.length
    this.maxCoordinate = JSON.stringify({ x: this.maxX - 1, y: this.maxY - 1 })
  }

  calculateTheLowestRisk () {
    let coordinateIsLast = false
    let i = 0
    this.close.set(JSON.stringify({ x: 0, y: 0 }), 1)
    this.open.set(JSON.stringify({ x: 1, y: 0 }), this.chitonRisks[0][1])
    this.open.set(JSON.stringify({ x: 0, y: 1 }), this.chitonRisks[1][0])

    while (!coordinateIsLast && i < 10000000) {
      coordinateIsLast = this.calculateTheNextRisk()
      i++
    }

    return this.open.get(this.maxCoordinate)
  }

  mapSort (a, b) {
    if (a[1] < b[1]) return -1
    if (a[1] > b[1]) return 1
    return 0
  }

  calculateTheNextRisk () {
    let entries = [...this.open.entries()].sort(this.mapSort)
    const actualPosition = entries.shift()
    const { x, y } = JSON.parse(actualPosition[0])
    const value = actualPosition[1]

    let neighborCoordinate = null
    let neighborOpenedValue = null

    this.open = new Map(entries)
    const neighSet = (nC, neighborValue) => {
      if (this.open.has(nC)) {
        neighborOpenedValue = this.open.get(nC)
        this.open.set(nC, neighborValue < neighborOpenedValue ? neighborValue : neighborOpenedValue)
        this.close.set(nC, neighborValue > neighborOpenedValue ? neighborValue : neighborOpenedValue)
      } else {
        this.open.set(nC, neighborValue)
      }
    }

    if (x > 0) {
      neighborCoordinate = JSON.stringify({ x: x - 1, y })
      if (!this.close.has(neighborCoordinate)) {
        neighSet(neighborCoordinate, value + this.chitonRisks[y][x - 1])
      }
    }

    if (y > 0) {
      neighborCoordinate = JSON.stringify({ x, y: y - 1 })
      if (!this.close.has(neighborCoordinate)) {
        neighSet(neighborCoordinate, value + this.chitonRisks[y - 1][x])
      }
    }

    if (x + 1 < this.maxX) {
      neighborCoordinate = JSON.stringify({ x: x + 1, y })
      if (!this.close.has(neighborCoordinate)) {
        neighSet(neighborCoordinate, value + this.chitonRisks[y][x + 1])
        if (neighborCoordinate === this.maxCoordinate) return true
      }
    }

    if (y + 1 < this.maxY) {
      neighborCoordinate = JSON.stringify({ x, y: y + 1 })
      if (!this.close.has(neighborCoordinate)) {
        neighSet(neighborCoordinate, value + this.chitonRisks[y + 1][x])
        if (neighborCoordinate === this.maxCoordinate) return true
      }
    }

    this.close.set(actualPosition[0], actualPosition[1])
    return false
  }
}

export default () => {
  const caveMap = new CaveMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day15.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  return caveMap.calculateTheLowestRisk()
}
