import fs from 'fs'
import path from 'path'

class Board {
  constructor (board) {
    this.numbers = new Map()
    this.rows = []
    this.columns = [[], [], [], [], []]
    let number = null
    let row = []

    board.forEach((line, i) => {
      row = []
      const {
        first,
        second,
        third,
        fourth,
        fifth,
      } = /^(?<first>[ |\d]\d) (?<second>[ |\d]\d) (?<third>[ |\d]\d) (?<fourth>[ |\d]\d) (?<fifth>[ |\d]\d)$/.exec(line)?.groups

      number = parseInt(first)
      this.numbers.set(number, { x: 0, y: i })
      row.push(number)
      this.columns[0].push(number)

      number = parseInt(second)
      this.numbers.set(number, { x: 1, y: i })
      row.push(number)
      this.columns[1].push(number)

      number = parseInt(third)
      this.numbers.set(number, { x: 2, y: i })
      row.push(number)
      this.columns[2].push(number)

      number = parseInt(fourth)
      this.numbers.set(number, { x: 3, y: i })
      row.push(number)
      this.columns[3].push(number)

      number = parseInt(fifth)
      this.numbers.set(number, { x: 4, y: i })
      row.push(number)
      this.columns[4].push(number)

      this.rows.push(row)
    })
  }

  selectNumber (number) {
    if (this.numbers.has(number)) {
      const { x, y } = this.numbers.get(number)
      this.rows[y] = this.rows[y].filter(rowNumber => rowNumber !== number)
      this.columns[x] = this.columns[x].filter(columnNumber => columnNumber !== number)
    }
  }

  boardWin () {
    return this.rows.some(row => row.length === 0) || this.columns.some(column => column.length === 0)
  }

  numbersSum () {
    return this.rows.reduce((sum, row) => sum + (row.length ? row.reduce((rowSum, number) => rowSum + number) : 0), 0)
  }
}

export default () => {
  const drawNumbers = []
  const boards = []
  let board = []
  let startI = null
  fs.readFileSync(path.join(process.cwd(), '/data/day4.txt'), { encoding: 'utf8' })
    .split('\n')
    .forEach((line, i) => {
      if (i === 0) {
        line.split(',')
            .forEach((numberDrawn) => {
              drawNumbers.push(parseInt(numberDrawn))
            })
      } else {
        if (line === '') {
          if (i !== 1) {
            boards.push(new Board(board))
          }
          startI = i
        } else {
          if (i === startI + 1) {
            board = []
          }
          board.push(line)
        }
      }
    })

  let boardWin = false
  let winningNumber = null
  let boardSum = null
  for (const drawNumber of drawNumbers) {
    boards.forEach(board => {
      board.selectNumber(drawNumber)
    })

    boardWin = boards.some(board => {
      if (board.boardWin()) {
        winningNumber = drawNumber
        boardSum = board.numbersSum()
        return true
      }

      return false
    })

    if (boardWin) {
      break
    }
  }

  return winningNumber * boardSum
}
