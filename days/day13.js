import fs from 'fs'
import path from 'path'

class Transparent {
  constructor (entries) {
    this.folds = []
    this.folded = []
    this.dots = new Set()

    for (const entry of entries) {
      if (entry === '') continue

      const coordinates = /^(?<x>\d+),(?<y>\d+)$/.exec(entry)
      if (coordinates) {
        const { x, y } = coordinates.groups
        this.dots.add(JSON.stringify({ x: parseInt(x), y: parseInt(y) }))
        continue
      }

      const fold = /^fold along (?<coordinate>[x|y])=(?<value>\d+)$/.exec(entry)
      if (fold) {
        const { value, coordinate } = fold.groups
        this.folds.push({ coordinate, value: parseInt(value) })
      }
    }
  }

  fold () {
    const { coordinate, value } = this.folds.shift()
    this.folded.push({ coordinate, value })
    const newSet = new Set()

    for (const dot of this.dots) {
      const { x, y } = JSON.parse(dot)
      const coordinateUpdated = coordinate === 'x' ? x : y

      if (coordinateUpdated < value) {
        newSet.add(JSON.stringify({ x, y }))
        continue
      }

      const diff = coordinateUpdated - value

      newSet.add(JSON.stringify(
        coordinate === 'x'
        ? { x: x - (diff * 2), y }
        : { x, y: y - (diff * 2) },
      ))
    }

    this.dots = newSet
  }

  getTransparentSize () {
    return this.dots.size
  }
}

export default () => {
  const transparent = new Transparent(
    fs.readFileSync(path.join(process.cwd(), '/data/day13.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  transparent.fold()

  return transparent.getTransparentSize()
}
