import fs from 'fs'
import path from 'path'

class DeepMap {
  constructor (positions) {
    this.positions = []
    positions.forEach(positionsLine => {
      if (positionsLine !== '') {
        this.positions.push(positionsLine.split('')
                                         .map(position => parseInt(position)))
      }
    })
  }

  calculateLowestPoints () {
    this.lowestPoint = []
    let top = null
    let left = null
    let right = null
    let bottom = null
    let depthPointPosition
    let yLength = this.positions.length
    let xLength = this.positions[0].length

    for (let y = 0; y < yLength; y++) {
      for (let x = 0; x < xLength; x++) {
        depthPointPosition = this.positions[y][x]

        if (y > 0) {
          top = this.positions[y - 1][x]
          if (top <= depthPointPosition) continue
        }
        if (x > 0) {
          left = this.positions[y][x - 1]
          if (left <= depthPointPosition) continue
        }
        if (y < yLength - 1) {
          bottom = this.positions[y + 1][x]
          if (bottom <= depthPointPosition) continue
        }
        if (x < xLength - 1) {
          right = this.positions[y][x + 1]
          if (right <= depthPointPosition) continue
        }

        this.lowestPoint.push({
          depth: depthPointPosition,
          position: { x, y },
        })
      }
    }
  }

  getTheLowestPointRiskLevelSum () {
    return this.lowestPoint.reduce((acc, position) => acc + position.depth + 1, 0)
  }
}

export default () => {
  const map = new DeepMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day9.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  map.calculateLowestPoints()

  return map.getTheLowestPointRiskLevelSum()
}
