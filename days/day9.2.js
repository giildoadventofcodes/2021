import fs from 'fs'
import path from 'path'

class DeepMap {
  constructor (positions) {
    this.positions = []
    positions.forEach(positionsLine => {
      if (positionsLine !== '') {
        this.positions.push(positionsLine.split('')
                                         .map(position => parseInt(position)))
      }
    })

    this.yLength = this.positions.length
    this.xLength = this.positions[0].length
  }

  readVerticalPositions (y, x, basinSize, toBottom) {
    if (this.coordinateAlreadyRead.has(JSON.stringify({ x, y }))) {
      return basinSize
    }
    if (this.positions[y][x] === 9) {
      this.coordinateAlreadyRead.add(JSON.stringify({ x, y }))
      return basinSize
    }

    this.coordinateAlreadyRead.add(JSON.stringify({ x, y }))
    basinSize++

    if (x > 0) basinSize = this.readHorizontalPositions(y, x - 1, basinSize, false)
    if (x < this.xLength - 1) basinSize = this.readHorizontalPositions(y, x + 1, basinSize, true)

    if (toBottom && y === this.yLength - 1) return basinSize
    if (!toBottom && y === 0) return basinSize

    return this.readVerticalPositions(toBottom ? y + 1 : y - 1, x, basinSize, toBottom)
  }

  readHorizontalPositions (y, x, basinSize, toRight) {
    if (this.coordinateAlreadyRead.has(JSON.stringify({ x, y }))) {
      return basinSize
    }
    if (this.positions[y][x] === 9) {
      this.coordinateAlreadyRead.add(JSON.stringify({ x, y }))
      return basinSize
    }

    this.coordinateAlreadyRead.add(JSON.stringify({ x, y }))
    basinSize++

    if (y > 0) basinSize = this.readVerticalPositions(y - 1, x, basinSize, false)
    if (y < this.yLength - 1) basinSize = this.readVerticalPositions(y + 1, x, basinSize, true)

    if (toRight && x === this.xLength - 1) return basinSize
    if (!toRight && x === 0) return basinSize

    return this.readHorizontalPositions(y, toRight ? x + 1 : x - 1, basinSize, toRight)
  }

  calculateBasinsSize () {
    this.coordinateAlreadyRead = new Set()
    this.basins = []

    let depthPointPosition

    for (let y = 0; y < this.yLength; y++) {
      for (let x = 0; x < this.xLength; x++) {
        if (this.coordinateAlreadyRead.has(JSON.stringify({ x, y }))) continue

        depthPointPosition = this.positions[y][x]

        if (depthPointPosition === 9) {
          this.coordinateAlreadyRead.add(JSON.stringify({ x, y }))
          continue
        }

        this.basins.push(this.readHorizontalPositions(y, x, 0, true))
      }
    }
  }

  getTheBasinsSize () {
    return this.basins.sort((a, b) => {
      if (a < b) return 1
      if (a > b) return -1
      return 0
    })
               .splice(0, 3)
               .reduce((acc, basin) => acc * basin, 1)
  }
}

export default () => {
  const map = new DeepMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day9.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  map.calculateBasinsSize()

  return map.getTheBasinsSize()
}
