import fs from 'fs'
import path from 'path'

class Polymerization {
  constructor (entries) {
    this.rules = new Map()
    this.polymer = new Map()

    for (const entry of entries) {
      if (entry === '') continue

      const transform = /^(?<pair>[A-Z][A-Z]) -> (?<insert>[A-Z])$/.exec(entry)
      if (transform) {
        const { insert, pair } = transform.groups

        this.rules.set(pair, insert)
      }

      if (transform === null) {
        let pair = ''
        for (let i = 1; i < entry.length; i++) {
          pair = entry[i - 1] + entry[i]
          this.polymer.set(
            pair,
            this.polymer.has(pair) ? this.polymer.get(pair) + 1 : 1,
          )
        }
      }
    }
  }

  polymerization (number) {
    let elementInserted = ''
    let newPolymer = new Map()
    for (let i = 0; i < number; i++) {
      newPolymer = new Map()
      for (const [pair, number] of this.polymer) {
        elementInserted = this.rules.get(pair)

        newPolymer.set(
          pair[0] + elementInserted,
          newPolymer.has(pair[0] + elementInserted)
          ? newPolymer.get(pair[0] + elementInserted) + number
          : number,
        )
        newPolymer.set(
          elementInserted + pair[1],
          newPolymer.has(elementInserted + pair[1])
          ? newPolymer.get(elementInserted + pair[1]) + number
          : number,
        )
      }
      this.polymer = newPolymer
    }
  }

  getElements () {
    const elements = new Map()
    for (const [pair, number] of this.polymer) {
      elements.set(
        pair[0],
        elements.has(pair[0]) ? elements.get(pair[0]) + number : number,
      )
      elements.set(
        pair[1],
        elements.has(pair[1]) ? elements.get(pair[1]) + number : number,
      )
    }

    const values = [...elements.values()].sort((a, b) => {
      if (a < b) return -1
      if (b > a) return 1
      return 0
    })

    return Math.ceil(values[values.length - 1] / 2) - Math.ceil(values[0] / 2)
  }
}

export default () => {
  const polymerization = new Polymerization(
    fs.readFileSync(path.join(process.cwd(), '/data/day14.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  polymerization.polymerization(40)

  return polymerization.getElements()
}
