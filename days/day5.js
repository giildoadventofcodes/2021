import fs from 'fs'
import path from 'path'

class HydrothermalMap {
  constructor () {
    this.coordinates = new Map()
  }

  addCoordinate (x, y) {
    const coordinates = JSON.stringify({ x, y })
    if (this.coordinates.has(coordinates)) {
      this.coordinates.set(coordinates, this.coordinates.get(coordinates) + 1)
    } else {
      this.coordinates.set(coordinates, 1)
    }
  }

  readRow (xStart, xEnd, y) {
    for (let i = xStart; i <= xEnd; i++) {
      this.addCoordinate(i, y)
    }
  }

  readColumn (yStart, yEnd, x) {
    for (let i = yStart; i <= yEnd; i++) {
      this.addCoordinate(x, i)
    }
  }

  readDiff (x1, x2, y1, y2) {
    x1 === x2
    ? y1 > y2 ? this.readColumn(y2, y1, x1) : this.readColumn(y1, y2, x1)
    : x1 > x2 ? this.readRow(x2, x1, y1) : this.readRow(x1, x2, y1)
  }

  getDangerousArea () {
    let dangerousArea = 0
    for (const [_, value] of this.coordinates) {
      if (value > 1) {
        dangerousArea++
      }
    }
    return dangerousArea
  }
}

export default () => {
  const map = new HydrothermalMap()
  fs.readFileSync(path.join(process.cwd(), '/data/day5.txt'), { encoding: 'utf8' })
    .split('\n')
    .forEach(line => {
      if (line) {
        const { x1, x2, y1, y2 } = /^(?<x1>\d+),(?<y1>\d+) -> (?<x2>\d+),(?<y2>\d+)$/.exec(line).groups
        if (x1 === x2 || y1 === y2) {
          map.readDiff(
            parseInt(x1),
            parseInt(x2),
            parseInt(y1),
            parseInt(y2),
          )
        }
      }
    })

  return map.getDangerousArea()
}
