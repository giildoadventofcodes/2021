import fs from 'fs'
import path from 'path'

class NavigationSystem {
  constructor (entries) {
    this.entries = []
    this.failedLines = []
    this.entriesCompletions = []
    this.openChars = ['(', '[', '{', '<']
    this.closeChars = [')', ']', '}', '>']
    entries.forEach(line => {
      if (line !== '') {
        this.entries.push(line.split(''))
      }
    })
  }

  linesChecker () {
    let checkChars = []
    let lastChar = ''

    return this.entries.reduce((acc, line, i) => {
      checkChars = []
      lastChar = ''
      for (const char of line) {
        if (this.openChars.includes(char)) {
          checkChars.push(char)
          continue
        }

        lastChar = checkChars.pop()

        switch (char) {
          case this.closeChars[0]:
            if (lastChar === this.openChars[0]) continue
            this.failedLines.push(i)
            return this.illegalCharPoint(char) + acc

          case this.closeChars[1]:
            if (lastChar === this.openChars[1]) continue
            this.failedLines.push(i)
            return this.illegalCharPoint(char) + acc

          case this.closeChars[2]:
            if (lastChar === this.openChars[2]) continue
            this.failedLines.push(i)
            return this.illegalCharPoint(char) + acc

          case this.closeChars[3]:
            if (lastChar === this.openChars[3]) continue
            this.failedLines.push(i)
            return this.illegalCharPoint(char) + acc
        }
      }

      return acc
    }, 0)
  }

  checkIncompletLines () {
    let checkChars = []
    let points = []
    let point = 0
    this.entries.filter((_, i) => !this.failedLines.includes(i))
        .forEach(line => {
          checkChars = []
          point = 0
          for (const char of line) {
            if (this.openChars.includes(char)) {
              checkChars.push(char)
              continue
            }

            checkChars.pop()
          }

          for (let i = checkChars.length - 1; i >= 0; i--) {
            point *= 5
            point += this.completionCharPoint(checkChars[i])
          }
          points.push(point)
        })

    points = points.sort((a, b) => {
      if (a < b) return -1
      if (a > b) return 1
      return 0
    })

    return points[Math.floor(points.length / 2)]
  }

  illegalCharPoint (illegalChar) {
    switch (illegalChar) {
      case this.closeChars[0]:
        return 3
      case this.closeChars[1]:
        return 57
      case this.closeChars[2]:
        return 1197
      case this.closeChars[3]:
        return 25137
    }
  }

  completionCharPoint (completionChar) {
    switch (completionChar) {
      case this.openChars[0]:
        return 1
      case this.openChars[1]:
        return 2
      case this.openChars[2]:
        return 3
      case this.openChars[3]:
        return 4
    }
  }
}

export default () => {
  const navigationSystem = new NavigationSystem(
    fs.readFileSync(path.join(process.cwd(), '/data/day10.txt'), { encoding: 'utf8' })
      .split('\n'),
  )
  navigationSystem.linesChecker()

  return navigationSystem.checkIncompletLines()
}
