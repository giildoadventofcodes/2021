import fs from 'fs'
import path from 'path'

class PositionsManager {
  constructor (positions) {
    this.positions = new Map()
    this.min = 0
    this.max = 0
    positions.forEach(pos => {
      if (this.min > pos) this.min = pos
      if (pos > this.max) this.max = pos
      const posMap = this.positions.get(pos)
      this.positions.set(pos, posMap ? posMap + 1 : 1)
    })
    this.average = Math.round(positions.reduce((sum, pos) => sum + pos, 0) / positions.length)
    this.bestPosition = this.average
  }

  getFuelConsumption (positionTested) {
    let consumption = 0
    for (const [position, crabNumber] of this.positions) {
      consumption += Math.abs(positionTested - position) * crabNumber
    }
    return consumption
  }

  calculateTheBestPosition () {
    let test = true
    let security = 0
    let minFuelConsumption = 0
    while (test && security < 10000) {
      const fuelConsumption = this.getFuelConsumption(this.bestPosition)
      const fuelConsumptionBefore = this.getFuelConsumption(this.bestPosition - 1)
      const fuelConsumptionAfter = this.getFuelConsumption(this.bestPosition + 1)

      if (fuelConsumption < fuelConsumptionBefore && fuelConsumption < fuelConsumptionAfter) {
        minFuelConsumption = fuelConsumption
        test = false
      } else if (fuelConsumptionBefore < fuelConsumption && fuelConsumptionBefore < fuelConsumptionAfter) {
        this.bestPosition--
      } else if (fuelConsumptionAfter < fuelConsumption && fuelConsumptionAfter < fuelConsumptionBefore) {
        this.bestPosition++
      } else {
        console.log('error')
        throw new Error('Pas de meilleure position !')
      }
      security++
    }

    return minFuelConsumption
  }
}

export default () => {
  let values = fs.readFileSync(path.join(process.cwd(), '/data/day7.txt'), { encoding: 'utf8' })
                   .split('\n')[0].split(',')
                                  .map(pos => parseInt(pos))

  const positionsManager = new PositionsManager(values)

  try {
    return positionsManager.calculateTheBestPosition()
  } catch (error) {
    return error
  }
}
