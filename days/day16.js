import fs from 'fs'
import path from 'path'

class Code {
  constructor (entries) {
    this.code = ''
    this.securityNumber = 100

    for (const entry of entries) {
      if (entry !== '') {
        this.code = entry.split('')
                         .map(hexChar => this.translateHexToBin(hexChar))
                         .join('')
      }
    }
    console.log(this.code)
  }

  translateHexToBin (hexChar) {
    return parseInt(hexChar, 16)
      .toString(2)
      .padStart(4, '0')
  }

  readPacket (sequence = null) {
    const packet = {
      body: null,
      children: [],
      literalFinishCursor: null,
      literalValue: null,
      operatorSetting: null,
      operatorType: null,
      sequence: null,
      typeId: null,
      version: null,
    }

    if (sequence) {
      packet.sequence = sequence
      packet.version = sequence.slice(0, 3)
      packet.typeId = sequence.slice(3, 6)
    } else {
      packet.sequence = this.code
      packet.version = this.code.slice(0, 3)
      packet.typeId = this.code.slice(3, 6)
    }

    if (packet.typeId === '100') {
      sequence
      ? packet.body = sequence.slice(6)
      : packet.body = this.code.slice(6)

      this.readLiteralPacket(packet)
    } else {
      if (sequence) {
        packet.operatorType = sequence[6]
        packet.body = sequence.slice(7)
      } else {
        packet.operatorType = this.code[6]
        packet.body = this.code.slice(7)
      }

      packet.operatorType === '0'
      ? this.readOperatorPacketType0(packet)
      : this.readOperatorPacketType1(packet)
    }

    return packet
  }

  readLiteralPacket (parentPacket) {
    let isFinalPacket = false
    let i = 0
    let packetRead = ''
    const packetsList = []
    const packetsValueList = []

    while (!isFinalPacket && i < this.securityNumber) {
      packetRead = parentPacket.body.slice(i * 5, (i + 1) * 5)

      packetsList.push(packetRead)
      packetsValueList.push(packetRead.slice(1, 5))

      isFinalPacket = packetRead[0] === '0'

      i++
    }

    parentPacket.literalFinishCursor = i
    parentPacket.body = packetsList.join('')
    parentPacket.literalValue = packetsValueList.join('')
  }

  readOperatorPacketType0 (parentPacket) {
    const operatorSetting = parseInt(parentPacket.body.slice(0, 15), 2)
    let newSequence = parentPacket.body.slice(15, operatorSetting + 15)
    let i = 0
    let newPacket = null
    let childSequence = null

    while (newSequence !== '' && i < this.securityNumber) {
      newPacket = this.readPacket(newSequence)
      parentPacket.children.push(newPacket)

      if (newPacket.literalFinishCursor) {
        newSequence = newSequence.slice(6 + 5 * newPacket.literalFinishCursor)
      } else {
        childSequence = this.getBodyPacketWithChildren(newPacket)
        console.log('index 0 ', newSequence.indexOf(childSequence), ' child ', childSequence, newPacket)
        newSequence = newSequence.slice(newSequence.indexOf(childSequence) + childSequence.length)
        console.log(newSequence)
        console.log('--------------------------------------------------------------')
      }

      if (/^0+$/.test(newSequence)) break

      i++
    }
  }

  readOperatorPacketType1 (parentPacket) {
    const operatorSetting = parseInt(parentPacket.body.slice(0, 11), 2)
    let newSequence = parentPacket.body.slice(11)
    let newPacket = null
    let childSequence = null

    for (let i = 0; i < operatorSetting; i++) {
      newPacket = this.readPacket(newSequence)
      parentPacket.children.push(newPacket)

      if (newPacket.literalFinishCursor) {
        newSequence = newSequence.slice(6 + 5 * newPacket.literalFinishCursor)
      } else {
        childSequence = this.getBodyPacketWithChildren(newPacket)
        console.log('index 1 ', newSequence.indexOf(childSequence), ' child ', childSequence)
        newSequence = newSequence.slice(newSequence.indexOf(childSequence) + childSequence.length)
        console.log(newSequence)
      }

      if (/^0+$/.test(newSequence)) break
    }
  }

  getBodyPacketWithChildren (parentPacket) {
    return parentPacket.children.reduce((sum, child) => {
      if (child.typeId === '100') {
        return sum + child.version + child.typeId + child.body
      }

      return sum + this.getBodyPacketWithChildren(child)
    }, '')
  }

  static getVersionSum (packetParent, isRoot) {
    if (isRoot) {
      return parseInt(packetParent.version, 2) + this.getVersionSum(packetParent, false)
    }

    return packetParent.children.reduce((sum, child) => {
      return parseInt(child.version, 2) + sum + this.getVersionSum(child, false)
    }, 0)
  }
}

export default () => {
  const code = new Code(
    fs.readFileSync(path.join(process.cwd(), '/data/day16.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  const packet = code.readPacket()

  return Code.getVersionSum(packet, true)
}
