import fs from 'fs'
import path from 'path'

const calculateUniqueFormat = (entry) => {
  const charNumber = entry.length
  return charNumber === 2 || charNumber === 3 || charNumber === 4 || charNumber === 7
}

export default () => {
  let i = 0
  const lines = fs.readFileSync(path.join(process.cwd(), '/data/day8.txt'), { encoding: 'utf8' })
                  .split('\n')
                  .filter(line => line !== '')
                  .map(line => {
                    return line.split('|')
                  })
                  .map(([before, after]) => {
                    return {
                      entry: before.split(' ')
                                   .filter(number => number !== ''),
                      output: after.split(' ')
                                   .filter(number => {
                                     if (number === '') return false

                                     if (calculateUniqueFormat(number)) i++

                                     return true
                                   }),
                    }
                  })

  return i
}
