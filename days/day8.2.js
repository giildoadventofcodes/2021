import fs from 'fs'
import path from 'path'

class Numbers {
  constructor (line) {
    const [entries, outputs] = line.split(' | ')

    this.entriesNumber = entries.split(' ')
                                .map(number => number.split(''))
    this.outputsNumber = outputs.split(' ')
                                .map(number => number.split(''))

    this.results = new Map([
      ['0', []],
      ['1', []],
      ['2', []],
      ['3', []],
      ['4', []],
      ['5', []],
      ['6', []],
      ['7', []],
      ['8', []],
      ['9', []],
    ])

    this.letters = new Map([
      ['a', ''],
      ['b', ''],
      ['c', ''],
      ['d', ''],
      ['e', ''],
      ['f', ''],
      ['g', ''],
    ])
    this.getA()
    this.getBEF()
    this.getCDG()
    this.initResultArray()
  }

  getA () {
    this.results.set('1', this.entriesNumber.find(number => number.length === 2))
    this.results.set('4', this.entriesNumber.find(number => number.length === 4))
    this.results.set('7', this.entriesNumber.find(number => number.length === 3))
    this.results.set('8', this.entriesNumber.find(number => number.length === 7))

    this.letters.set(
      'a',
      this.results.get('7')
          .find(char => (
            !this.results.get('4')
                 .includes(char) &&
            !this.results.get('1')
                 .includes(char)
          )),
    )
  }

  getBEF () {
    const letters = new Map([
      ['a', 0],
      ['b', 0],
      ['c', 0],
      ['d', 0],
      ['e', 0],
      ['f', 0],
      ['g', 0],
    ])

    for (const values of this.entriesNumber) {
      if (values.includes('a')) letters.set('a', letters.get('a') + 1)
      if (values.includes('b')) letters.set('b', letters.get('b') + 1)
      if (values.includes('c')) letters.set('c', letters.get('c') + 1)
      if (values.includes('d')) letters.set('d', letters.get('d') + 1)
      if (values.includes('e')) letters.set('e', letters.get('e') + 1)
      if (values.includes('f')) letters.set('f', letters.get('f') + 1)
      if (values.includes('g')) letters.set('g', letters.get('g') + 1)
    }

    for (const [letter, occurrenceNumber] of letters) {
      switch (occurrenceNumber) {
        case 4:
          this.letters.set('e', letter)
          break
        case 6:
          this.letters.set('b', letter)
          break
        case 9:
          this.letters.set('f', letter)
          break
      }
    }
  }

  getCDG () {
    this.letters.set(
      'c',
      this.results.get('1')
          .find(char => {
            return this.results.get('4')
                       .includes(char) &&
              this.results.get('7')
                  .includes(char) &&
              this.letters.get('f') !== char
          }),
    )
    this.letters.set(
      'd',
      this.results.get('4')
          .find(char => {
            return !this.results.get('7')
                        .includes(char) &&
              !this.results.get('1')
                   .includes(char) &&
              this.letters.get('b') !== char
          }),
    )
    this.letters.set(
      'g',
      this.results.get('8')
          .find(char => {
            return !this.results.get('1')
                        .includes(char) &&
              !this.results.get('4')
                   .includes(char) &&
              !this.results.get('7')
                   .includes(char) &&
              this.letters.get('e') !== char
          }),
    )
  }

  initResultArray () {
    this.resultsArrays = new Map([
      [
        '0',
        [
          this.letters.get('a'),
          this.letters.get('b'),
          this.letters.get('c'),
          this.letters.get('e'),
          this.letters.get('f'),
          this.letters.get('g'),
        ],
      ],
      [
        '2',
        [
          this.letters.get('a'),
          this.letters.get('c'),
          this.letters.get('d'),
          this.letters.get('e'),
          this.letters.get('g'),
        ],
      ],
      [
        '3',
        [
          this.letters.get('a'),
          this.letters.get('c'),
          this.letters.get('d'),
          this.letters.get('f'),
          this.letters.get('g'),
        ],
      ],
      [
        '5',
        [
          this.letters.get('a'),
          this.letters.get('b'),
          this.letters.get('d'),
          this.letters.get('f'),
          this.letters.get('g'),
        ],
      ],
      [
        '6',
        [
          this.letters.get('a'),
          this.letters.get('b'),
          this.letters.get('d'),
          this.letters.get('e'),
          this.letters.get('f'),
          this.letters.get('g'),
        ],
      ],
      [
        '9',
        [
          this.letters.get('a'),
          this.letters.get('b'),
          this.letters.get('c'),
          this.letters.get('d'),
          this.letters.get('f'),
          this.letters.get('g'),
        ],
      ],
    ])
  }

  getResults () {
    return this.outputsNumber.map(outputNumber => {
      if (outputNumber.length === 2) return '1'
      if (outputNumber.length === 3) return '7'
      if (outputNumber.length === 4) return '4'
      if (outputNumber.length === 7) return '8'

      if (outputNumber.every(char => this.resultsArrays.get('2')
                                         .includes(char))) return 2
      if (outputNumber.every(char => this.resultsArrays.get('3')
                                         .includes(char))) return 3
      if (outputNumber.every(char => this.resultsArrays.get('5')
                                         .includes(char))) return 5
      if (outputNumber.every(char => this.resultsArrays.get('0')
                                         .includes(char))) return 0
      if (outputNumber.every(char => this.resultsArrays.get('6')
                                         .includes(char))) return 6
      if (outputNumber.every(char => this.resultsArrays.get('9')
                                         .includes(char))) return 9
    })
               .join('')
  }
}

const calculateUniqueFormat = (entry) => {
  const charNumber = entry.length
  return charNumber === 2 || charNumber === 3 || charNumber === 4 || charNumber === 7
}

export default () => {
  return fs.readFileSync(path.join(process.cwd(), '/data/day8.txt'), { encoding: 'utf8' })
           .split('\n')
           .filter(line => line !== '')
           .map(line => new Numbers(line))
           .reduce((acc, numbers) => acc + parseInt(numbers.getResults()), 0)
}
