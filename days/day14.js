import fs from 'fs'
import path from 'path'

class Polymerization {
  constructor (entries) {
    this.rules = new Map()
    this.polymer = ''

    for (const entry of entries) {
      if (entry === '') continue

      const transform = /^(?<pair>[A-Z][A-Z]) -> (?<insert>[A-Z])$/.exec(entry)
      if (transform) {
        const { insert, pair } = transform.groups

        this.rules.set(pair, insert)
      }

      if (transform === null) {
        this.polymer = entry
      }
    }
  }

  polymerization (number) {
    let newPolymer = ''
    let elementInserted = ''
    for (let i = 0; i < number; i++) {
      newPolymer = ''
      for (let j = 1; j < this.polymer.length; j++) {
        elementInserted = this.rules.get(this.polymer[j - 1] + this.polymer[j])

        if (j === 1) {
          newPolymer = this.polymer[j - 1]
        }

        newPolymer += elementInserted + this.polymer[j]
      }
      this.polymer = newPolymer
    }
  }

  getElements () {
    const elements = new Map()
    for (const polymerElement of this.polymer) {
      elements.set(
        polymerElement,
        elements.has(polymerElement) ? elements.get(polymerElement) + 1 : 1,
      )
    }

    const values = [...elements.values()].sort((a, b) => {
      if (a < b) return -1
      if (b > a) return 1
      return 0
    })
    return values[values.length - 1] - values[0]
  }
}

export default () => {
  const polymerization = new Polymerization(
    fs.readFileSync(path.join(process.cwd(), '/data/day14_test.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  polymerization.polymerization(10)

  return polymerization.getElements()
}
