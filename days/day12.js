import fs from 'fs'
import path from 'path'

class SubmarineSubterraneanMap {
  constructor (entries) {
    this.subterraneans = new Map([
      ['start', { isBigSub: false, neighbors: new Set() }],
      ['end', { isBigSub: false, neighbors: new Set() }],
    ])
    this.littleSubterraneanAlreadyVisited = new Set()
    this.possibilitiesPath = new Set()
    for (const line of entries) {
      if (line !== '') {
        if (/^start-[a-zA-Z]+$/.test(line)) {
          const [_, subName] = line.split('-')
          this.addSub('start', subName, false)
          continue
        }

        if (/^[a-zA-Z]+-start$/.test(line)) {
          const [subName, _] = line.split('-')
          this.addSub('start', subName, false)
          continue
        }

        if (/^end-[a-zA-Z]+$/.test(line)) {
          const [_, subName] = line.split('-')
          this.addSub('end', subName, true)
          continue
        }

        if (/^[a-zA-Z]-end+$/.test(line)) {
          const [subName, _] = line.split('-')
          this.addSub('end', subName, true)
          continue
        }

        const [subNameA, subNameB] = line.split('-')
        this.addSub(subNameA, subNameB, true)
      }
    }
  }

  addSub (subA, subB, addReverse) {
    if (this.subterraneans.has(subA)) {
      const value = this.subterraneans.get(subA)
      this.subterraneans.set(
        subA,
        {
          ...value,
          neighbors: value.neighbors.add(subB),
        },
      )
    } else {
      this.subterraneans.set(
        subA,
        {
          isBigSub: /^[A-Z]+$/.test(subA),
          neighbors: new Set([subB]),
        },
      )
    }

    if (addReverse) {
      const value = this.subterraneans.get(subB)
      if (this.subterraneans.has(subB)) {
        this.subterraneans.set(
          subB,
          {
            ...value,
            neighbors: value.neighbors.add(subA),
          },
        )
      } else {
        this.subterraneans.set(
          subB,
          {
            isBigSub: /^[A-Z]+$/.test(subB),
            neighbors: new Set([subA]),
          },
        )
      }
    }
  }

  browseTheMap () {
    const startNeighbors = this.subterraneans.get('start').neighbors

    for (const startNeighbor of startNeighbors) {
      this.browseNeighborsSubterranean(startNeighbor, ['start', startNeighbor], true)
    }
  }

  browseNeighborsSubterranean (subName, path, isStartSub) {
    const subNeighbors = this.subterraneans.get(subName).neighbors
    let subNeighborSettings = null
    for (const subNeighborName of subNeighbors) {
      if (isStartSub) {
        subNeighborSettings = this.subterraneans.get(subName)
        this.littleSubterraneanAlreadyVisited = new Set([subNeighborSettings.isBigSub ? null : subName])
      }

      if (subNeighborName === 'end') {
        this.possibilitiesPath.add([...path, 'end'])
        continue
      }

      subNeighborSettings = this.subterraneans.get(subNeighborName)
      if (!subNeighborSettings.isBigSub && this.littleSubterraneanAlreadyVisited.has(subNeighborName)) {
        continue
      } else {
        if (!subNeighborSettings.isBigSub) this.littleSubterraneanAlreadyVisited.add(subNeighborName)
        this.browseNeighborsSubterranean(subNeighborName, [...path, subNeighborName], false)
        if (!subNeighborSettings.isBigSub) this.littleSubterraneanAlreadyVisited.delete(subNeighborName)
      }
    }

    return
  }
}

export default () => {
  const submarineSubterraneanMap = new SubmarineSubterraneanMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day12.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  submarineSubterraneanMap.browseTheMap()
  return submarineSubterraneanMap.possibilitiesPath.size
}
