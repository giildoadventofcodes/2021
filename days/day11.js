import fs from 'fs'
import path from 'path'

class OctopusMap {
  constructor (entries) {
    this.lines = []
    this.octopusAlreadyHighlighted = new Set()
    this.numberOfOctopusHighlighted = 0
    entries.forEach(line => {
      if (line !== '') {
        this.lines.push(line.split('')
                            .map(energyLevel => parseInt(energyLevel)))
      }
    })
  }

  nextStep () {
    this.octopusAlreadyHighlighted = new Set()
    for (let y = 0; y < this.lines.length; y++) {
      for (let x = 0; x < this.lines[y].length; x++) {
        this.lines[y][x]++
      }
    }
    for (let y = 0; y < this.lines.length; y++) {
      for (let x = 0; x < this.lines[y].length; x++) {
        if (this.lines[y][x] > 9) {
          this.adjacentOctopusIncrement(x, y)
        }
      }
    }
    for (let y = 0; y < this.lines.length; y++) {
      for (let x = 0; x < this.lines[y].length; x++) {
        if (this.lines[y][x] >= 10) {
          this.lines[y][x] = 0
          this.numberOfOctopusHighlighted++
        }
      }
    }
  }

  adjacentOctopusIncrement (x, y) {
    if (!this.octopusAlreadyHighlighted.has(JSON.stringify({ x, y }))) {
      this.octopusAlreadyHighlighted.add(JSON.stringify({ x, y }))
      if (x > 0 && y > 0) {
        this.lines[y - 1][x - 1]++
        if (this.lines[y - 1][x - 1] === 10) {
          this.adjacentOctopusIncrement(x - 1, y - 1)
        }
      }

      if (y > 0) {
        this.lines[y - 1][x]++
        if (this.lines[y - 1][x] === 10) {
          this.adjacentOctopusIncrement(x, y - 1)
        }
      }

      if (x < this.lines[0].length - 1 && y > 0) {
        this.lines[y - 1][x + 1]++
        if (this.lines[y - 1][x + 1] === 10) {
          this.adjacentOctopusIncrement(x + 1, y - 1)
        }
      }

      if (x < this.lines[0].length - 1) {
        this.lines[y][x + 1]++
        if (this.lines[y][x + 1] === 10) {
          this.adjacentOctopusIncrement(x + 1, y)
        }
      }

      if (x < this.lines[0].length - 1 && y < this.lines.length - 1) {
        this.lines[y + 1][x + 1]++
        if (this.lines[y + 1][x + 1] === 10) {
          this.adjacentOctopusIncrement(x + 1, y + 1)
        }
      }

      if (y < this.lines.length - 1) {
        this.lines[y + 1][x]++
        if (this.lines[y + 1][x] === 10) {
          this.adjacentOctopusIncrement(x, y + 1)
        }
      }

      if (x > 0 && y < this.lines.length - 1) {
        this.lines[y + 1][x - 1]++
        if (this.lines[y + 1][x - 1] === 10) {
          this.adjacentOctopusIncrement(x - 1, y + 1)
        }
      }

      if (x > 0) {
        this.lines[y][x - 1]++
        if (this.lines[y][x - 1] === 10) {
          this.adjacentOctopusIncrement(x - 1, y)
        }
      }
    }
  }

  calculateXSteps (stepNumber) {
    for (let i = 0; i < stepNumber; i++) {
      this.nextStep()
    }

    return this.numberOfOctopusHighlighted
  }
}

export default () => {
  const octopus = new OctopusMap(
    fs.readFileSync(path.join(process.cwd(), '/data/day11.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  return octopus.calculateXSteps(100)
}
