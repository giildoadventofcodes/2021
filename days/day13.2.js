import fs from 'fs'
import path from 'path'

class Transparent {
  constructor (entries) {
    this.folds = []
    this.folded = []
    this.dots = new Set()

    for (const entry of entries) {
      if (entry === '') continue

      const coordinates = /^(?<x>\d+),(?<y>\d+)$/.exec(entry)
      if (coordinates) {
        const { x, y } = coordinates.groups
        this.dots.add(JSON.stringify({ x: parseInt(x), y: parseInt(y) }))
        continue
      }

      const fold = /^fold along (?<coordinate>[x|y])=(?<value>\d+)$/.exec(entry)
      if (fold) {
        const { value, coordinate } = fold.groups
        this.folds.push({ coordinate, value: parseInt(value) })
      }
    }
  }

  allFold () {
    for (let i = 0; i < this.folds.length; i++) {
      this.fold(i)
    }
  }

  fold (i) {
    const { coordinate, value } = this.folds[i]
    const newSet = new Set()

    for (const dot of this.dots) {
      const { x, y } = JSON.parse(dot)
      const coordinateUpdated = coordinate === 'x' ? x : y

      if (coordinateUpdated < value) {
        newSet.add(JSON.stringify({ x, y }))
        continue
      }

      const diff = coordinateUpdated - value

      newSet.add(JSON.stringify(
        coordinate === 'x'
        ? { x: x - (diff * 2), y }
        : { x, y: y - (diff * 2) },
      ))
    }

    this.dots = newSet
  }

  getTransparentSize () {
    return this.dots.size
  }

  readLetters () {
    let dots = []
    for (const dot of this.dots) {
      dots.push(JSON.parse(dot))
    }

    const lines = []
    let line = ''
    for (let y = 0; y < 6; y++) {
      line = ''
      for (let x = 0; x < 40; x++) {
        line += dots.find(dot => dot.x === x && dot.y === y) ? '■' : ' '
      }
      lines.push(line)
    }

    console.log(lines)
  }
}

export default () => {
  const transparent = new Transparent(
    fs.readFileSync(path.join(process.cwd(), '/data/day13.txt'), { encoding: 'utf8' })
      .split('\n'),
  )

  transparent.allFold()
  transparent.readLetters()

  return transparent.getTransparentSize()
}
